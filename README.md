# extractor
Downloads, extracts, and compiles Quark binaries. Used in the plugin IDE environment.
# Note
You're better off compiling your own Quark binaries if you're not using the plugin. It's strictly for plugin use.
